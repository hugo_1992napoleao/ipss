var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/home', {
            templateUrl: 'views/home.html'
            //,controller: 'home'
        })
        .when('/newArtigo', {
            templateUrl: 'views/addArtigo.html',
            controller: 'addArtigo'
        })
        .when('/newUser', {
            templateUrl:'views/addUser.html',
            controller: 'addUser'
        })
        .when('/listArtigo', {
            templateUrl:'views/listarArtigos.html',
            controller: 'listarArtigos'
        }).when('/licitar:id', {
            templateUrl:'views/licitar.html',
            controller: 'licitar'
        }).when('/userDetalhes:id', {
            templateUrl:'views/userDetalhes.html',
            controller: 'userDetalhes'
        }).when('/gerir', {
            templateUrl:'views/gerir.html',
            controller: 'gerir'
        }).when('/sobre', {
            templateUrl:'views/sobre.html'
        }).when('/dashboard', {
            templateUrl:'views/dashboard.html',
            controller:'dashboard'
        }).when('/dashboard1', {
            templateUrl:'views/dashboard1.html',
            controller:'dashboard1'
        })
        .otherwise({
            redirectTo: '/home'
        });
}]);

myApp.controller('home', ['$scope', '$http', '$cookies', function($scope, $http, $cookies) {
    console.log(1);
    $scope.Logar = function() {        
        $http({method:'GET', url:'http://localhost:3000/utilizador/'} ).then(function(response){
            for(var i = 0; i < response.length; i++) {
                if(response.data.utilizador[i].name === $scope.userName && response.data.utilizador[i].name === $scope.password) {
                    $cookies('userId', response.data.utilizador[i]._id);
                }
            }
        });
    }
}]);

myApp.controller('addArtigo', ['$scope', '$http', function($scope, $http){
   
    $scope.add = function() {

        var esquema = {
            "name": $scope.name,
            "endSale": $scope.endSale,
            "img": $scope.img,
            "description": $scope.description,
            "price": $scope.price,
            "category": $scope.category,
            "ownerId": '5d0bd5761c9d4400007ca4fe'
        };
        console.log(esquema);
        $http({
            method:'POST',
            url: 'http://localhost:3000/artigo/',
            data: esquema
        });
    }
}]);

myApp.controller('addUser', ['$scope', '$http', function($scope, $http){
    $scope.enviado = false;
    console.log($scope.enviado);
    $scope.add = function() {

        $scope.enviado = true;
        console.log($scope.enviado);
        var esquema = {
            "name": $scope.userName,
            "password": $scope.password,
            "email": $scope.email
        };

        
        $http({
            method:'POST',
            url: 'http://localhost:3000/utilizador/',
            data: esquema
        });
    }
}]);

myApp.controller('listarArtigos', ['$scope', '$http', function($scope, $http){
    $scope.artigos = [];
    $scope.myArtigos = function(id) {
        $http({method:'GET', url:'http://localhost:3000/artigo/'}).then(function(response) {
            for (let i = 0; i <= response.data.count; i++) {                
                    $scope.artigos.pop();
            }
            for (let i = 0; i < response.data.count; i++) {
            if (response.data.artigo[i].ownerId._id === id) {
                $scope.artigos.push(response.data.artigo[i]);
            } 
        }
    });
    }

    $scope.myBides = function(id) {
        $http({method:'GET', url:'http://localhost:3000/bide/'}).then(function(response) {
            for (let i = 0; i <= response.data.count; i++) {
                    $scope.artigos.pop();                
            }
            for (let i = 0; i < response.data.count; i++) {
            if (response.data.bide[i].owner._id === id) {
                $scope.artigos.push(response.data.bide[i].artigo);
            } 
        }
    });
    }

    

    $http({method:'GET', url:'http://localhost:3000/artigo'}).then(function(response) {
        $scope.artigos = response.data.artigo;
    });
}]);

myApp.controller('userDetalhes', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){
    var id = $routeParams.id;

    $http.get('http://localhost:3000/utilizador/' + id.substring(1)).then(function(response) {
        $scope.user = response.data.utilizador;
    });

    $scope.Editar = function(id) {
        console.log(id);
        var esquema = [];
        if ($scope.userName != null) {
            esquema.push({"propName": "name", "value": $scope.userName});
        }

        if ($scope.password != null) {
            esquema.push({"propName": "password", "value": $scope.password});
        }

        if ($scope.email != null) {
            esquema.push({"propName": "email", "value": $scope.email});
        }        

        $http({
            method:'PATCH',
            url: 'http://localhost:3000/utilizador/' + id,
            data: esquema
        });
    };

}]);

myApp.controller('licitar', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){
    var id = $routeParams.id;

    $http.get('http://localhost:3000/bide/').then(function(response) {
        $scope.bides = response.data.bide;
    });

    $http.get('http://localhost:3000/artigo/' + id.substring(1)).then(function(response) {
        $scope.artigo = response.data.artigo;
    });
    $scope.paypal = function(userId, artigoId){
        var esquema = {
            "owner": userId,
            "artigo": artigoId,
            "value": $scope.myBide,
        }
        console.log(esquema);
        $http({
            method:'POST',
            url: 'http://localhost:3000/bide/',
            data: esquema
        }).then(response => {
            console.log(response);
        });

        $http({
            method:'POST',
            url: 'http://localhost:3000/bides/',
            data: esquema
        });
    }; 
    
    $scope.EditarArtigo = function(id) {
        var esquema = [];
        if ($scope.price != null) {
            esquema.push({"propName": "price", "value": $scope.price});
        }

        if ($scope.endSale != null) {
            esquema.push({"propName": "endSale", "value": $scope.endSale});
        }

        if ($scope.winner != null) {
            esquema.push({"propName": "winner", "value": $scope.winner});
        }        

        $http({
            method:'PATCH',
            url: 'http://localhost:3000/artigo/' + id,
            data: esquema
        });
    };
}]);

myApp.controller('gerir', ['$scope', '$http', function($scope, $http){
    
    $http.get('http://localhost:3000/artigo/').then(function(response) {
        $scope.artigos = response.data.artigo;
    });

    $http.get('http://localhost:3000/utilizador/').then(function(response) {
        $scope.users = response.data.utilizador;
    });

    $http.get('http://localhost:3000/bide/').then(function(response) {
        $scope.bides = response.data.bide;
    });


    $scope.apagarUser = function(id) {
        $http({
            method:'DELETE',
            url:'http://localhost:3000/utilizador/' + id
        });
    };
    $scope.apagarBide = function(id) {
        $http({
            method:'DELETE',
            url:'http://localhost:3000/bide/' + id
        });
    };
    $scope.apagarArtigo = function(id) {
        $http({
            method:'DELETE',
            url:'http://localhost:3000/artigo/' + id
        });
    };


}]);

myApp.controller('dashboard', ['$scope', '$http', function($scope, $http){

    $http({method:'GET', url:'http://localhost:3000/artigo'}).then(function(response1) {
        $http({method:'GET', url:'http://localhost:3000/bide/'}).then(function(response2) {
            $http({method:'GET', url:'http://localhost:3000/utilizador/'}).then(function(response3) {
            console.log(response1);
            console.log(response2);
            console.log(response3);
                let contLeiloesTerminados = 0;
                let montaanteAdquirido = 0;
                let valorTotalGasto = 0;
                for(let i = 0; i < response1.data.count; i++) {
                    if(response1.data.artigo[i].winnerId._id != "5d0bd5761c9d4400007ca4fe") {
                        contLeiloesTerminados++;
                    }
                }
                let contLeiloesativos = 0;
                for(let i = 0; i < response1.data.count; i++) {
                    if(response1.data.artigo[i].winnerId._id == "5d0bd5761c9d4400007ca4fe") {
                        contLeiloesativos++;
                    }
                }
                montaanteAdquirido= response2.data.count;
                let totalGasto = 0;
                for(let i = 0; i < response1.data.count; i++) {
                    if(response1.data.artigo[i].winnerId._id != "5d0bd5761c9d4400007ca4fe") {
                        totalGasto = totalGasto + response1.data.artigo[i].price;
                    }
                }
                
                totalGasto = totalGasto + montaanteAdquirido;

    let myChart = document.getElementById('myChart').getContext('2d');

Chart.defaults.global.defaultsFontFamily = 'Lato';  
Chart.defaults.global.defaultsFontSize = 18;
Chart.defaults.global.defaultsFontColor = '#777';

let massPopChart = new Chart(myChart,{
    type:'pie', //bar
    data:{
        labels:['Leiloes Terminados','Leiloes Ativos','Montante Instituição','Montante Gasto'],
        datasets:[{
            label:'Population',
            data:[
                contLeiloesTerminados,
                contLeiloesativos,
                montaanteAdquirido,
                totalGasto
            ],
            //backgroundColor:'blue'
            backgroundColor:[
                'rgba(255,99,132,0.6)',
                'rgba(54,162,235,0.6)',
                'rgba(255,206,86,0.6)',
                'rgba(75,192,192,0.6)',
                'rgba(153,102,255,0.6)',
                'rgba(255,159,64,0.6)',
                'rgba(255,99,132,0.6)'
            ],
            borderWidth:4,
            borderColor:'#777',
            hoverBorderWidth:3,
            hoverBorderColor:'#000'
        }]
    },
    options:{
        title:{
            display:true,
            text:'Informações Gerais',
            fontSize:25
        },
        legend:{
            display:true,
           //position:'botton',
            labels:{
                fontColor:'#000'
            }
        },
        layout:{
            padding:{
                left:50,
                rigth:0,
                botton:0,
                top:0
            }
        },
        tooltips:{
            enable:true
        }
    }
});
});
});
});
}]);

myApp.controller('dashboard1', ['$scope', '$http', function($scope, $http){
    $http({method:'GET', url:'http://localhost:3000/artigo'}).then(function(response1) {
        $http({method:'GET', url:'http://localhost:3000/bide/'}).then(function(response2) {
            $http({method:'GET', url:'http://localhost:3000/utilizador/'}).then(function(response3) {

            let nArtigosSubmetidos = 0;
            console.log(response1.data.artigo);
            for (let i = 0; i < response1.data.count; i++) {
                if(response1.data.artigo[i].ownerId._id == "5d0a233138143c428c08a9e3") {
                    nArtigosSubmetidos++;
                }
            }
            let nLancesLeilao = 0;
            for(let i = 0; i < response2.data.count; i++) {
                if(response2.data.bide[i].owner._id == "5d0a233138143c428c08a9e3") {
                    nLancesLeilao++;
                }
            }
            let nganhos = 0;
            for (let i = 0; i < response1.data.count; i++) {
                if(response1.data.artigo[i].winnerId._id == "5d0a233138143c428c08a9e3") {
                    nganhos++;
                }
            }
    let myChart = document.getElementById('myChart').getContext('2d');

Chart.defaults.global.defaultsFontFamily = 'Lato';
Chart.defaults.global.defaultsFontSize = 18;
Chart.defaults.global.defaultsFontColor = '#777';

let massPopChart = new Chart(myChart,{
    type:'bar', //bar
    data:{
        labels:['Produtos Submetidos','Lances','Leiloes Ganhos'],

        datasets:[{
            label:'Valores',
            data:[
                nArtigosSubmetidos,
                nLancesLeilao,
                nganhos
            ],
            //backgroundColor:'blue'
            backgroundColor:[
                'rgba(255,99,132,0.6)',
                'rgba(54,162,235,0.6)',
                'rgba(255,206,86,0.6)',
                'rgba(75,192,192,0.6)',
                'rgba(153,102,255,0.6)',
                'rgba(255,159,64,0.6)',
                'rgba(255,99,132,0.6)'
            ],
            borderWidth:4,
            borderColor:'#777',
            hoverBorderWidth:3,
            hoverBorderColor:'#000'

        }]
    },
    options:{
        title:{
            display:true,
            text:'Informações Gerais',
            fontSize:25
        },
        legend:{
            display:true,
           //position:'botton',
            labels:{
                fontColor:'#000'
            }
        },
        layout:{
            padding:{
                left:50,
                rigth:0,
                botton:0,
                top:0
            }
        },
        tooltips:{
            enable:true
        }
    }
});

});
});
});
}]);