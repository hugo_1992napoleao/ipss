import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Chart } from "chart.js";
import { Apartamento } from "../apartamento";
import { ApartamentoService } from "../apartamento.service";
import { HistoricoService } from "../historico.service";
import { UserService } from "../user.service";
@Component({
  selector: "app-dashboard-admin",
  templateUrl: "./dashboard-admin.component.html",
  styleUrls: ["./dashboard-admin.component.css"]
})
export class DashboardAdminComponent {
  chart: any = [];
  opt: any = {};
  @ViewChild("myCanvas", { static: false }) myCanvas: ElementRef;
  @ViewChild("myCanvas2", { static: false }) myCanvas2: ElementRef;
  public context: CanvasRenderingContext2D;

  constructor(
    private apartamento: ApartamentoService,
    private historico: HistoricoService,
    private user: UserService
  ) {}

  ngAfterViewInit() {
    this.gettest();
    this.gettest1();
  }

  gettest(): void {
    this.apartamento.getApartamentoFromAPI().subscribe(res => {
      this.apartamento.getApartamentoAtivosFromAPI().subscribe(res2 => {
        this.historico.getHistoricoFromAPI().subscribe(res3 => {
          console.log(res["artigo"][0]);
          let ocupacao = 0;

          for (var i = 0; i < res["count"]; i++) {
            if (res["artigo"][i].estado != "disponivel") {
              ocupacao++;
            }
          }
          console.log(res3["count"]);
          let somatorio = 0;
          for (var i = 0; i < res3["count"]; i++) {
            somatorio = somatorio + res3["historico"][i].apartamentoId.preco;
          }
          this.context = (<HTMLCanvasElement>(
            this.myCanvas.nativeElement
          )).getContext("2d");
          this.chart = new Chart(this.context, {
            type: "pie",
            data: {
              labels: [
                "Taxa de ocupação",
                "Numero de alugueres",
                "Montante Gasto"
              ],
              datasets: [
                {
                  label: "Population",
                  data: [ocupacao, res3["count"], somatorio],
                  //backgroundColor:'blue'
                  backgroundColor: [
                    "rgba(255,99,132,0.6)",
                    "rgba(54,162,235,0.6)",
                    "rgba(255,206,86,0.6)",
                    "rgba(75,192,192,0.6)",
                    "rgba(153,102,255,0.6)",
                    "rgba(255,159,64,0.6)",
                    "rgba(255,99,132,0.6)"
                  ],
                  borderWidth: 4,
                  borderColor: "#777",
                  hoverBorderWidth: 3,
                  hoverBorderColor: "#000"
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: "Informações Gerais",
                fontSize: 25
              },
              legend: {
                display: true,
                //position:'botton',
                labels: {
                  fontColor: "#000"
                }
              }
            }
          });
        });
      });
    });
  }

  gettest1(): void {
    this.user.getUsersFromAPI().subscribe(res => {
      let admin = 0;
      for (var i = 0; i < res["count"]; i++) {
        console.log(res['utilizador'][i].type)
        if (res['utilizador'][i].type != "cliente") {
          admin = admin + 1;
        }
      }
      this.context = (<HTMLCanvasElement>(
        this.myCanvas2.nativeElement
      )).getContext("2d");
      this.chart = new Chart(this.context, {
        type: "bar",
        data: {
          labels: ["Numero de clientes", "Numero de adminstradores"],
          datasets: [
            {
              label: "Utilizadores",
              data: [admin, res['count'] - admin],
              //backgroundColor:'blue'
              backgroundColor: [
                "rgba(255,99,132,0.6)",
                "rgba(54,162,235,0.6)",
                "rgba(255,206,86,0.6)",
                "rgba(75,192,192,0.6)",
                "rgba(153,102,255,0.6)",
                "rgba(255,159,64,0.6)",
                "rgba(255,99,132,0.6)"
              ],
              borderWidth: 4,
              borderColor: "#777",
              hoverBorderWidth: 3,
              hoverBorderColor: "#000"
            }
          ]
        },
        options: {
          title: {
            display: true,
            text: "Informações Utilizadores",
            fontSize: 25
          },
          legend: {
            display: true,
            //position:'botton',
            labels: {
              fontColor: "#000"
            }
          }
        }
      });
    });
  }
}
