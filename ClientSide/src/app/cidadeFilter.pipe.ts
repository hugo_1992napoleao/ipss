import { PipeTransform, Pipe } from '@angular/core';
import { Apartamento } from './apartamento';

@Pipe({
    name: 'cidadeFilter'
})
export class cidadeFilterPipe implements PipeTransform {
    transform(apartamentos: Apartamento[], search: string): Apartamento[] {
        if (!apartamentos || !search) {
            return apartamentos
        }
        return apartamentos.filter(apartamentos => 
            apartamentos.morada.cidade.toLowerCase().indexOf(search.toLowerCase()) !== -1)
    }
}