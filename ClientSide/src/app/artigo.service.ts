import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Apartamento } from './apartamento';

@Injectable({
  providedIn: "root"
})
export class ArtigoService {
  private _URL: string = "http://localhost:3000/apartamento/";

  constructor(private http: HttpClient) {}

  getApartamentoFromAPI(): Observable<Apartamento[]> {
    return this.http.get<Apartamento[]>(this._URL);
  }

  newApartamentoToAPI(data: Apartamento): Observable<any> {
    return this.http.post<any>(this._URL, data);
  }

  getApartamentobyId(id): Observable<Apartamento[]> {
    return this.http.get<Apartamento[]>(this._URL + id);
  }

  deleteApartamento(id:string) {
    return this.http.delete<any>(this._URL + id);
  }

  updateApartamento(id:string, data) {
    return this.http.patch<any>(this._URL + id, data);
  }
/*
  leiloesAtivos(): Observable<IArtigo[]> {
    return this.http.get<IArtigo[]>(this._URL + '/ativos');
  }

  myArtigos(id:string): Observable<IArtigo[]> {
    return this.http.get<IArtigo[]>(this._URL + '/myArtigos/' + id);
  }*/
}
