export class Apartamento {
    constructor(
        public nome: String,
        public morada: {
          cidade: String,
          rua: String
        },
        public lotacao: number,
        public preco: number,
        public caracteristicas: Array<String>,
        public servicos: Array<String>,
        public estado: string,
        public dono: string,
        public multimedia: string
      ) {}
}
