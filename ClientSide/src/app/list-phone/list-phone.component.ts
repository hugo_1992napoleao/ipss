import { Component, OnInit } from "@angular/core";
import { ApartamentoService } from "../apartamento.service";
import { Router } from "@angular/router";
import { HistoricoService } from "../historico.service";

@Component({
  selector: "app-list-phone",
  templateUrl: "./list-phone.component.html",
  styleUrls: ["./list-phone.component.css"]
})
export class ListPhoneComponent implements OnInit {
  public artigos = [];
  search: string;
  private userId = sessionStorage.getItem("id");
  constructor(
    private _apartamentoService: ApartamentoService,
    private router: Router,
    private _historicoService: HistoricoService
  ) {}

  ngOnInit() {
    this._apartamentoService.getApartamentoAtivosFromAPI().subscribe(data => {
      this.artigos = data["artigo"];
    });
  }
  detalhes(artigo) {
    console.log(artigo.id);
    if (artigo.id == undefined) {
      this.router.navigate(["/licitar", artigo._id]);
    } else {
      this.router.navigate(["/licitar", artigo.id]);
    }
  }

  myReservas() {
    this._historicoService
      .getHistoricobyIdUser(sessionStorage.getItem("id"))
      .subscribe(data => {
        this.artigos = data["historico"];
        this.artigos = [];
        let temp;
        for (let i = 0; i < data["count"]; i++) {
          temp = this.pesquisa(data["historico"][i].apartamentoId);
          if (temp == false) {
            this.artigos.push(data["historico"][i].apartamentoId);
          }
        }
      });
  }

  pesquisa(elemento) {
    for (let i = 0; i < this.artigos.length; i++) {
      if (this.artigos[i].id == elemento.id) {
        return true;
      }
    }
    return false;
  }
}
