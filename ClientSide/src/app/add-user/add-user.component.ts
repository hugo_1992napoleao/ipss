import { Component, OnInit } from "@angular/core";
import { User } from "../user";
import { UserService } from "../user.service";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.css"]
})
export class AddUserComponent implements OnInit {
  user: User = new User("", "", "", "");
  comfirm: string;

  constructor(private _User: UserService) {}

  ngOnInit() {}

  newUser() {
    if (this.user.name.length <= 0 || this.user.name.length >= 30) {
      window.alert("user incorrecto");
    } else if (this.user.email.length <= 0 || this.user.email.length >= 50) {
      window.alert("email incorrecto");
    } else if (
      this.user.password != this.comfirm ||
      this.user.password.length <= 0 ||
      this.user.password.length > 50
    ) {
      window.alert("A confirmaçao nao está correta");
    } else {
      this._User.newUser(this.user).subscribe(data => {
        window.alert("Utilizador criado com sucesso");
        console.log("sucesso: ", data);
      }),
        error => console.error("fail: ", error);      
    }
  }
}
