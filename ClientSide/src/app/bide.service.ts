import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IBide } from './IBide';
import { Observable } from 'rxjs';
import { Bide } from './bide';


@Injectable({
  providedIn: 'root'
})
export class BideService {

  private _URL: string = "http://localhost:3000/bide/";

  constructor(private http: HttpClient) { }

  getBideFromAPI(): Observable<IBide[]> {
    return this.http.get<IBide[]>(this._URL);
  }

  newBide(bide: Bide): Observable<Bide> {
    return this.http.post<any>(this._URL, bide);
  }

  deleteBide(id:string) {
    return this.http.delete<any>(this._URL + id);
  }

  getArtigoBide(id:string) {
    return this.http.get<any>(this._URL + 'bideArtigo/' + id);
  }

  myBide(id:string) {
    return this.http.get<any>(this._URL + 'bideuser/' + id);
  }
}
