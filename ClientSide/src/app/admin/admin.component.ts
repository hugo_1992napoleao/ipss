import { Component, OnInit } from "@angular/core";
import { ArtigoService } from "../artigo.service";
import { Router } from "@angular/router";
import { UserService } from "../user.service";
import { BideService } from "../bide.service";
import { HistoricoService } from '../historico.service';
import { ApartamentoService } from '../apartamento.service';

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"]
})
export class AdminComponent implements OnInit {
  public apartamentos = [];
  public users = [];
  public historico = [];

  constructor(
    private _apartamentoService: ApartamentoService,
    private router: Router,
    private _UsersService: UserService,
    private _historicoServico: HistoricoService
  ) {}

  ngOnInit() {
    this._apartamentoService.getApartamentobyUserId(sessionStorage.getItem('email')).subscribe(data => {
      this.apartamentos = data["artigo"];
    });

    this._UsersService.getUsersFromAPI().subscribe(data => {
      this.users = data["utilizador"];
    });

    this._historicoServico.getHistoricoFromAPI().subscribe(data => {
      for(let i = 0; i < data['count']; i++) {
        if (data['historico'][i].apartamentoId.dono == sessionStorage.getItem('email')) {
          this.historico.push(data['historico'][i]);
        }
      }
    });
  }

  detalhesArtigo(artigo) {
    this.router.navigate(["/phone-info", artigo._id]);
  }

  detalhesUser(user) {
    this.router.navigate(["/userDetalhes", user._id]);
  }

  loadDashboard() {
    this.router.navigate(["/dashboard-admin"]);
  }

  apagarUser(user) {
    this._UsersService.deleteUser(user._id).subscribe(data => {
      console.log("sucesso: ", data);
    }),
      error => console.error("fail: ", error);
  }

  apagarArtigo(artigo) {
    this._apartamentoService.deleteApartamento(artigo.id).subscribe(data => {
      console.log("sucesso: ", data);
    }),
      error => console.error("fail: ", error);
  }

  detalhesHistory(hist) {
    console.log(hist.id)
    this.router.navigate(["/history-info", hist.id]);
  }

  apagarHistory(hist) {
    this._historicoServico.deleteHistorico(hist.id).subscribe(data => {
      console.log("sucesso: ", data);
    }),
      error => console.error("fail: ", error);
  }
  
}
