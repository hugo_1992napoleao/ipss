import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private _loginUrl = "http://localhost:3000/utilizador";

  constructor(private http: HttpClient) {}

  getUserDetails(email, password) {
    return this.http
      .post(this._loginUrl, {
        email,
        password
      })
  }

  loginUser(loginCredenciais) {
    return this.http.post<any>(this._loginUrl + "/logar", loginCredenciais);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  loggedAdmin() {
    if (sessionStorage.getItem('tipo') == 'admin') {
      return true;
    } else {
      return false;
    }
  }

  loggout(){
    localStorage.removeItem('token');
    sessionStorage.removeItem('tipo');
  }
}
