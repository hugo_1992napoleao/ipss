export interface Historico {
    _id: string,
    userId: string,
    apartamentoId: string,
    inicio: String,
    fim: String,
    estado: String
}
