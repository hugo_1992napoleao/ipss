import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: "root"
})
export class AuthAdminGuard implements CanActivate {
  constructor(private auth: AuthService, private _router: Router) {}

  canActivate(): boolean {
    if (this.auth.loggedAdmin()) {
      return true;
    } else {
      this._router.navigate(["/home"]);
      return false;
    }
  }
}
