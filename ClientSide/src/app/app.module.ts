import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule, routingComponents } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TopNavComponent } from "./top-nav/top-nav.component";
import { HttpClientModule } from "@angular/common/http";
import { ArtigoService } from "./artigo.service";
import { FormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts";
import { AuthService } from "./auth.service";
import { AuthGuard } from "./auth.guard";
import { AuthAdminGuard } from "./auth-admin.guard";
import { cidadeFilterPipe } from "./cidadeFilter.pipe";
import {
  ScheduleModule,
  RecurrenceEditorModule,
  DayService,
  WeekService,
  WorkWeekService,
  MonthService,
  AgendaService
} from "@syncfusion/ej2-angular-schedule";

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    TopNavComponent,
    cidadeFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule,
    ScheduleModule,
    RecurrenceEditorModule
  ],
  providers: [
    ArtigoService,
    AuthService,
    AuthGuard,
    AuthAdminGuard,
    DayService,
    WeekService,
    WorkWeekService,
    MonthService,
    AgendaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
