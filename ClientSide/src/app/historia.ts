export class Historia {
    constructor(
        public _id: string,
        public userId: string,
        public apartamentoId: string,
        public inicio: String,
        public fim: String,
        public estado: String) {}
}
