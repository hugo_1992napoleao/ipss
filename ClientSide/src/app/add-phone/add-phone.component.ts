import { Component, OnInit } from "@angular/core";
import { ApartamentoService } from "../apartamento.service";
import { Apartamento } from "../apartamento";

@Component({
  selector: "app-add-phone",
  templateUrl: "./add-phone.component.html",
  styleUrls: ["./add-phone.component.css"]
})
export class AddPhoneComponent implements OnInit {
  selectedFile: File = null;

  caracteristicas = new Array();
  servicos = new Array();
  apartamento: Apartamento = new Apartamento(
    "",
    { cidade: "", rua: "" },
    0,
    0,
    this.caracteristicas,
    this.servicos,
    "",
    sessionStorage.getItem("email"),
    ""
  );

  constructor(private artigo: ApartamentoService) {}

  ngOnInit() {}

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
    console.log(event.target.files[0]);
  }

  newApartamento() {
    
    var elevador = <HTMLInputElement>document.getElementById("Elevador");
    if (elevador.checked == true) {
      this.caracteristicas.push("Elevador");
    }
    var Varanda = <HTMLInputElement>document.getElementById("Varanda");
    if (Varanda.checked == true) {
      this.caracteristicas.push("Varanda");
    }

    var terrraco = <HTMLInputElement>document.getElementById("terrraco");
    if (terrraco.checked == true) {
      this.caracteristicas.push("terrraço");
    }

    var Mobilado = <HTMLInputElement>document.getElementById("Mobilado");
    if (Mobilado.checked == true) {
      this.caracteristicas.push("Mobilado");
    }

    var deficientes = <HTMLInputElement>document.getElementById("deficientes");
    if (deficientes.checked == true) {
      this.caracteristicas.push("Suporte para deficientes");
    }

    var Internet = <HTMLInputElement>document.getElementById("Internet");
    if (Internet.checked == true) {
      this.servicos.push("Internet");
    }

    var Ar_Condicionado = <HTMLInputElement>document.getElementById("Ar_Condicionado");
    if (Ar_Condicionado.checked == true) {
      this.servicos.push("Ar Condicionado");
    }

    var Lareira = <HTMLInputElement>document.getElementById("Lareira");
    if (Lareira.checked == true) {
      this.servicos.push("Lareira");
    }

    var Telefone = <HTMLInputElement>document.getElementById("Telefone");
    if (Telefone.checked == true) {
      this.servicos.push("Telefone");
    }

    var TV = <HTMLInputElement>document.getElementById("TV");
    if (TV.checked == true) {
      this.servicos.push("TV Cabo");
    }
    var estado = <HTMLInputElement>document.getElementById("estado");
      this.apartamento.estado = estado.value;
      console.log(estado.value);

    //const fd = new FormData();
    //fd.append("image", this.selectedFile, this.selectedFile.name);

    if (this.apartamento.nome.length < 3 || this.apartamento.nome.length > 50) {
      window.alert('nome tem de ter mais de 3 carateres e menos de 50');
    } else if (this.apartamento.morada.cidade.length < 3 || this.apartamento.morada.cidade.length > 50) {
      window.alert('cidade tem de ter mais de 3 carateres e menos de 50');
    } else if (this.apartamento.morada.rua.length < 3 || this.apartamento.morada.rua.length > 50) {
      window.alert('rua tem de ter mais de 3 carateres e menos de 50');
    } else if (this.apartamento.preco <= 0) {
      window.alert('o preço tem que ser mais que 0');
    } else if (this.apartamento.lotacao <= 0 || this.apartamento.lotacao > 10) {
      window.alert('a lotação tem que ser mais que 0 e menor 10');
    } else if (this.apartamento.estado !== 'indisponivel' && this.apartamento.estado !== 'disponivel') {
      window.alert('Estado inválido');
    } else {
      this.artigo.newApartamentoToAPI(this.apartamento).subscribe(data => {
        window.alert('Apartamento criado com sucesso');
        console.log("sucesso: ",  data);
      }),
        error => console.error("fail: ", error);
    }


    
    
  }
}
