import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicitarComponent } from './licitar.component';

describe('LicitarComponent', () => {
  let component: LicitarComponent;
  let fixture: ComponentFixture<LicitarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicitarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicitarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
