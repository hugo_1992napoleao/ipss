import { Component, OnInit, AfterViewChecked } from "@angular/core";
import { ApartamentoService } from "../apartamento.service";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Apartamento } from "../apartamento";
import { HistoricoService } from "../historico.service";
import { Historico } from "../historico";
import { Historia } from "../historia";
import { DataAux } from "../data";

declare var paypal: any;

@Component({
  selector: "app-licitar",
  templateUrl: "./licitar.component.html",
  styleUrls: ["./licitar.component.css"]
})
export class LicitarComponent implements OnInit, AfterViewChecked {
  dataHoje: DataAux;
  artigo = {};
  id = "";
  histEntry: Historia = new Historia(
    "",
    sessionStorage.getItem("id"),
    this.id,
    "",
    "",
    "pendente"
  );
  apartamento: Apartamento;
  histo: Historico[];
  addScript: boolean = false;
  finalAmount: number = 1;

  paypalConfig = {
    env: "sandbox",
    client: {
      sandbox:
        "ASQ7UGEUzultTyi-ASdt-Mkiih6j5acfBtuxhmopQsN0WapbB4sS5ExwiL3X-2EqN28kAJRwyEhypZwe",
      production: "<my-production-key here>"
    },
    commit: true,
    payment: (data, actions) => {
      return actions.payment.create({
        payment: {
          transactions: [
            { amount: { total: this.finalAmount, currency: "EUR" } }
          ]
        }
      });
    },
    onAuthorize: (data, actions) => {
      return actions.payment.execute().then(payment => {
        this.newHistory();
      });
    }
  };

  constructor(
    private _historicoService: HistoricoService,
    private _apartamentoService: ApartamentoService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get("id");
    });

    this._apartamentoService.getApartamentobyId(this.id).subscribe(data => {
      this.apartamento = data["apartamento"];
    });
    this._historicoService
      .getHistoricobyApartamentoId(this.id)
      .subscribe(data => {
        this.histo = data["historico"];
      });
  }

  ngAfterViewChecked(): void {
    if (!this.addScript) {
      this.addPaypalScript().then(() => {
        paypal.Button.render(this.paypalConfig, "#paypal-checkout-btn");
      });
    }
  }
  addPaypalScript() {
    this.addScript = true;
    return new Promise((resolve, reject) => {
      let scripttagElement = document.createElement("script");
      scripttagElement.src = "https://www.paypalobjects.com/api/checkout.js";
      scripttagElement.onload = resolve;
      document.body.appendChild(scripttagElement);
    });
  }

  FormataStringData(data) {
    var dia = data.split("-")[2];
    var mes = data.split("-")[1];
    var ano = data.split("-")[0];

    return new DataAux(dia, mes, ano);
  }

  ComparaDatas(dataInici: DataAux, dataFim: DataAux) {
    if (dataInici.ano > dataFim.ano) {
      return 1;
    } else if (dataInici.ano < dataFim.ano) {
      return -1;
    } else {
      if (dataInici.mes > dataFim.mes) {
        return 1;
      } else if (dataInici.mes < dataFim.mes) {
        return -1;
      } else {
        if (dataInici.dia > dataFim.dia) {
          return 1;
        } else if (dataInici.dia < dataFim.dia) {
          return -1;
        } else {
          return 0;
        }
      }
    }
  }

  newHistory() {
    this.dataHoje = new DataAux(
      new Date().getDay(),
      new Date().getMonth() + 1,
      new Date().getFullYear()
    );

    let dataInicio = this.FormataStringData(this.histEntry.inicio);
    let dataFim = this.FormataStringData(this.histEntry.fim);

    if (this.ComparaDatas(dataInicio, this.dataHoje) < 1) {
      window.alert(
        "a data de inicio tem de ser posterior ou igual a data atual"
      );
    } else if (this.ComparaDatas(dataFim, this.dataHoje) < 0) {
      window.alert("a data de Fim tem de ser posterior ou igual a data atual");
    } else if (this.ComparaDatas(dataInicio, dataFim) == 1) {
      window.alert("a data de Fim tem de ser posterior ou igual a data inicio");
    } else {
      this.validar(dataInicio, dataFim);
    }
  }

  validar(dataInicio: DataAux, dataFim: DataAux) {
    let temp;
    this.histEntry.apartamentoId = this.id;
    for (let i = 0; i < this.histo.length; i++) {
      temp = false;
      if (
        this.ComparaDatas(
          this.FormataStringData(this.histo[i].inicio),
          dataInicio
        ) == 1 &&
        this.ComparaDatas(
          this.FormataStringData(this.histo[i].inicio),
          dataFim
        ) == 1
      ) {
        temp = true;
      }
      if (
        this.ComparaDatas(
          this.FormataStringData(this.histo[i].fim),
          dataInicio
        ) == -1
      ) {
        temp = true;
      }
    }
    if (temp == true) {
      this.histEntry.inicio = dataInicio.ano + "-"  + dataInicio.mes + '-' + dataInicio.dia;
      this.histEntry.fim = dataFim.ano + "-"  + dataFim.mes + '-' + dataFim.dia;
      this._historicoService
        .newHistoricoToAPI(this.histEntry)
        .subscribe(res => {
          window.alert("reserva efectuada");
        });
    } else {
      window.alert("Escolha outra data");
    }
  }
}
