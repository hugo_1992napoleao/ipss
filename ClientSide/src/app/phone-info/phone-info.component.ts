import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Apartamento } from '../apartamento';
import { ApartamentoService } from '../apartamento.service';

@Component({
  selector: 'app-phone-info',
  templateUrl: './phone-info.component.html',
  styleUrls: ['./phone-info.component.css']
})
export class PhoneInfoComponent implements OnInit {
  id: any;
  item: Apartamento;
  updateOps = [];
  array = [];
  selectedFile: File = null;

  caracteristicas = new Array();
  servicos = new Array();
  apartamento: Apartamento = new Apartamento(
    "",
    { cidade: "", rua: "" },
    0,
    0,
    this.caracteristicas,
    this.servicos,
    "",
    sessionStorage.getItem("id"),
    ""
  );

  constructor(private _apartamentoService: ApartamentoService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get("id");
    });

    this._apartamentoService.getApartamentobyId(this.id).subscribe(data => {
      console.log(data)
      this.item = data["apartamento"];
    });
  }


  
  updateArtigo() {
    var elevador = <HTMLInputElement>document.getElementById("Elevador");
    if (elevador.checked == true) {
      this.caracteristicas.push("Elevador");
    }
    var Varanda = <HTMLInputElement>document.getElementById("Varanda");
    if (Varanda.checked == true) {
      this.caracteristicas.push("Varanda");
    }

    var terrraco = <HTMLInputElement>document.getElementById("terrraco");
    if (terrraco.checked == true) {
      this.caracteristicas.push("terrraço");
    }

    var Mobilado = <HTMLInputElement>document.getElementById("Mobilado");
    if (Mobilado.checked == true) {
      this.caracteristicas.push("Mobilado");
    }

    var deficientes = <HTMLInputElement>document.getElementById("deficientes");
    if (deficientes.checked == true) {
      this.caracteristicas.push("Suporte para deficientes");
    }

    var Internet = <HTMLInputElement>document.getElementById("Internet");
    if (Internet.checked == true) {
      this.servicos.push("Internet");
    }

    var Ar_Condicionado = <HTMLInputElement>document.getElementById("Ar_Condicionado");
    if (Ar_Condicionado.checked == true) {
      this.servicos.push("Ar Condicionado");
    }

    var Lareira = <HTMLInputElement>document.getElementById("Lareira");
    if (Lareira.checked == true) {
      this.servicos.push("Lareira");
    }

    var Telefone = <HTMLInputElement>document.getElementById("Telefone");
    if (Telefone.checked == true) {
      this.servicos.push("Telefone");
    }

    var TV = <HTMLInputElement>document.getElementById("TV");
    if (TV.checked == true) {
      this.servicos.push("TV Cabo");
    }

    if (this.apartamento.nome != "") {
      this.updateOps.push({ propName: "nome", value: this.apartamento.nome });
    }

    if (this.apartamento.morada.cidade != "") {
      this.updateOps.push({
        propName: "cidade",
        value: this.apartamento.morada.cidade
      });
    }

    if (this.apartamento.morada.rua != "") {
      this.updateOps.push({
        propName: "rua",
        value: this.apartamento.morada.rua
      });
    }

    if (this.apartamento.preco > 0) {
      this.updateOps.push({
        propName: "preco",
        value: this.apartamento.preco
      });
    }

    if (this.apartamento.lotacao > 0) {
      this.updateOps.push({
        propName: "lotacao",
        value: this.apartamento.lotacao
      });
    }

    if (this.apartamento.estado != "") {
      this.updateOps.push({ propName: "estado", value: this.apartamento.estado });
    }

    if (this.caracteristicas.length > 0) {
      this.updateOps.push({ propName: "caracteristicas", value: this.apartamento.caracteristicas });
    }

    if (this.servicos.length > 0) {
      this.updateOps.push({ propName: "servicos", value: this.apartamento.servicos });
    }

    var removerS = <HTMLInputElement>document.getElementById("removerServiços");
    if (removerS.checked == true) {
      this.updateOps.push({ propName: "servicos", value: this.array });
    }

    var removerC = <HTMLInputElement>document.getElementById("removerCara");
    if (removerC.checked == true) {
      this.updateOps.push({ propName: "caracteristicas", value: this.array });
    }

    this._apartamentoService.updateApartamento(this.id, this.updateOps).subscribe(
      data => {
        console.log("sucesso: ",  data);
        window.alert('Apartamento atualizado com sucesso');

      }),
        error => console.error("fail: ", error);
  }
}
