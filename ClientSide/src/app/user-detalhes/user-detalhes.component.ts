import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router, ParamMap } from "@angular/router";
import { UserService } from "../user.service";
import { User } from "../user";

@Component({
  selector: "app-user-detalhes",
  templateUrl: "./user-detalhes.component.html",
  styleUrls: ["./user-detalhes.component.css"]
})
export class UserDetalhesComponent implements OnInit {
  id = "";
  utilizador: any;
  updateOps = [];
  comfirm: string;
  userModel: User = new User("", "", "", "");

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _userService: UserService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get("id");
    });

    this._userService.getUserbyId(this.id).subscribe(data => {
      this.utilizador = data["utilizador"];
    });
  }

  updateUser() {
    var type = <HTMLInputElement>document.getElementById("type");
    this.userModel.type = type.value;
    console.log(this.userModel.type);

    if (this.userModel.type != "") {
      this.updateOps.push({ propName: "type", value: this.userModel.type });
    }
    if (this.userModel.name != "") {
      this.updateOps.push({ propName: "name", value: this.userModel.name });
    }

    if (
      this.userModel.password != "" &&
      this.userModel.password == this.comfirm
    ) {
      this.updateOps.push({
        propName: "password",
        value: this.userModel.password
      });
    }

    if (this.userModel.email != "") {
      this.updateOps.push({ propName: "email", value: this.userModel.email });
    }

    

    this._userService
      .updateUser("http://localhost:3000/utilizador/" + this.id, this.updateOps)
      .subscribe(data => {
        window.alert("Utilizador atualizado com sucesso");
        console.log("sucesso: ", this.userModel);
      }),
      error => console.error("fail: ", error);
  }
}
