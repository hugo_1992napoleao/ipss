import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUsers } from './IUsers';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _URL: string = "http://localhost:3000/utilizador/";
  constructor(private http: HttpClient) { }

  getUsersFromAPI(): Observable<IUsers[]> {
    return this.http.get<IUsers[]>(this._URL);
  }

  getUserbyId(id): Observable<IUsers[]> {
    return this.http.get<IUsers[]>(this._URL + "/id/" + id);
  }

  newUser(user: User): Observable<User> {
    return this.http.post<any>(this._URL, user);
  }

  deleteUser(id:string) {
    return this.http.delete<any>(this._URL + id);
  }

  updateUser(url, user:any) {
    return this.http.patch<any>(url, user);
  }
}
