import { Component, OnInit } from '@angular/core';
import { AuthService } from "../auth.service"
import { from } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _authService: AuthService) { }

  ngOnInit() {
  }

  loggout() {
    this._authService.loggout();
  }

}
