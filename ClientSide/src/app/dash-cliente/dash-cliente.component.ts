import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { HistoricoService } from "../historico.service";
import { Chart } from "chart.js";

@Component({
  selector: "app-dash-cliente",
  templateUrl: "./dash-cliente.component.html",
  styleUrls: ["./dash-cliente.component.css"]
})
export class DashClienteComponent {
  chart: any = [];
  opt: any = {};
  @ViewChild("myCanvas", { static: false }) myCanvas: ElementRef;
  @ViewChild("myCanvas2", { static: false }) myCanvas2: ElementRef;
  public context: CanvasRenderingContext2D;

  constructor(private historico: HistoricoService) {}

  ngAfterViewInit() {
    this.gettest();
    this.gettest2();
  }

  gettest(): void {
    this.historico.getHistoricoFromAPI().subscribe(res => {
      let id = sessionStorage.getItem('id');
      let somatorio = 0;
      
      for (let i = 0; i < res['count']; i++) {
        console.log(res['historico'][i].userId)
        console.log(id)
        if(res['historico'][i].userId._id == id) {
          
          somatorio = somatorio + res['historico'][i].apartamentoId.preco;
        }
      }

      this.context = (<HTMLCanvasElement>(
        this.myCanvas.nativeElement
      )).getContext("2d");
      this.chart = new Chart(this.context, {
        type: "pie",
        data: {
          labels: ["Total gasto"],
          datasets: [
            {
              label: "Population",
              data: [somatorio],
              //backgroundColor:'blue'
              backgroundColor: [
                "rgba(255,99,132,0.6)",
                "rgba(54,162,235,0.6)",
                "rgba(255,206,86,0.6)",
                "rgba(75,192,192,0.6)",
                "rgba(153,102,255,0.6)",
                "rgba(255,159,64,0.6)",
                "rgba(255,99,132,0.6)"
              ],
              borderWidth: 4,
              borderColor: "#777",
              hoverBorderWidth: 3,
              hoverBorderColor: "#000"
            }
          ]
        },
        options: {
          title: {
            display: true,
            text: "Informações Gerais",
            fontSize: 25
          },
          legend: {
            display: true,
            //position:'botton',
            labels: {
              fontColor: "#000"
            }
          }
        }
      });
    });
  }

  gettest2(): void {
    this.historico.getHistoricoFromAPI().subscribe(res => {
      let id = sessionStorage.getItem('id');
      let count = 0;
      
      for (let i = 0; i < res['count']; i++) {
        console.log(res['historico'][i].userId)
        console.log(id)
        if(res['historico'][i].userId._id == id) {
          count++;
        }
      }

      this.context = (<HTMLCanvasElement>(
        this.myCanvas2.nativeElement
      )).getContext("2d");
      this.chart = new Chart(this.context, {
        type: "bar",
        data: {
          labels: ["Total reservas"],
          datasets: [
            {
              label: "Reservas",
              data: [count],
              //backgroundColor:'blue'
              backgroundColor: [
                "rgba(54,162,235,0.6)",
                "rgba(255,206,86,0.6)",
                "rgba(75,192,192,0.6)",
                "rgba(153,102,255,0.6)",
                "rgba(255,159,64,0.6)",
                "rgba(255,99,132,0.6)"
              ],
              borderWidth: 4,
              borderColor: "#777",
              hoverBorderWidth: 3,
              hoverBorderColor: "#000"
            }
          ]
        },
        options: {
          title: {
            display: true,
            text: "Informações Reserva",
            fontSize: 25
          },
          legend: {
            display: true,
            //position:'botton',
            labels: {
              fontColor: "#000"
            }
          }
        }
      });
    });
  }
}
