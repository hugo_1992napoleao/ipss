import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddPhoneComponent } from "./add-phone/add-phone.component";
import { SobreComponent } from "./sobre/sobre.component";
import { AddUserComponent } from "./add-user/add-user.component";
import { LicitarComponent } from "./licitar/licitar.component";
import { ListPhoneComponent } from "./list-phone/list-phone.component";
import { EditPhoneComponent } from "./edit-phone/edit-phone.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { AdminComponent } from "./admin/admin.component";
import { UserDetalhesComponent } from "./user-detalhes/user-detalhes.component";
import { DashboardAdminComponent } from "./dashboard-admin/dashboard-admin.component";
import { PhoneInfoComponent } from "./phone-info/phone-info.component";
import { AuthGuard } from "./auth.guard";
import { AuthAdminGuard } from "./auth-admin.guard";
import { DashClienteComponent } from './dash-cliente/dash-cliente.component';
import { HistoryInfoComponent } from './history-info/history-info.component';

const routes: Routes = [
  { path: "newPhone", component: AddPhoneComponent, canActivate: [AuthAdminGuard] },
  { path: "about", component: SobreComponent, canActivate: [AuthAdminGuard] },
  { path: "newUser", component: AddUserComponent },
  {
    path: "licitar/:id",
    component: LicitarComponent,
    canActivate: [AuthGuard]
  },
  { path: "listPhone", component: ListPhoneComponent },
  {
    path: "editPhone",
    component: EditPhoneComponent,
    canActivate: [AuthGuard]
  },
  { path: "login", component: LoginComponent },
  { path: "home", component: HomeComponent },
  { path: "admin", component: AdminComponent, canActivate: [AuthAdminGuard] },
  {
    path: "userDetalhes/:id",
    component: UserDetalhesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "dashboard-admin",
    component: DashboardAdminComponent,
    canActivate: [AuthAdminGuard]
  },
  {
    path: "history-info/:id",
    component: HistoryInfoComponent,
    canActivate: [AuthAdminGuard]
  },
  {
    path: "dash-cliente",
    component: DashClienteComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "phone-info/:id",
    component: PhoneInfoComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [
  AddPhoneComponent,
  SobreComponent,
  AddUserComponent,
  LicitarComponent,
  ListPhoneComponent,
  EditPhoneComponent,
  LoginComponent,
  HomeComponent,
  AdminComponent,
  UserDetalhesComponent,
  DashboardAdminComponent,
  PhoneInfoComponent,
  HistoryInfoComponent,
  DashClienteComponent
];
