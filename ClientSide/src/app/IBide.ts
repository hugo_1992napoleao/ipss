export interface IBide {
    _id: string,
    owner: string,
    artigo: string
    value: number,
    date: Date
}