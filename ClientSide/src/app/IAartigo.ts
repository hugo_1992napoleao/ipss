export interface IArtigo {
    id: String,
    name:String,
    endSale: Date,
    img: String,
    price: number,
    description: String,
    winner: String,
    estado: String,
    owner: String
} 