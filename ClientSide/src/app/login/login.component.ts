import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginUserData = {};
  constructor(private auth: AuthService, private _router: Router) { }

  ngOnInit() {
  }

  loginUser() {
    console.log(this.loginUserData)
    this.auth.loginUser(this.loginUserData).subscribe(
      res => {
        console.log(res);
        localStorage.setItem('token', res.token);
        sessionStorage.setItem('tipo', res.tipo);
        sessionStorage.setItem('id', res.id);
        sessionStorage.setItem('email', res.email);
        this._router.navigate(['/home']);
      },
      error => console.log(error)
    );  
  }

}
