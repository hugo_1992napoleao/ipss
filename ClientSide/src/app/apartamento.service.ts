import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Apartamento } from './apartamento';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApartamentoService {
  private _URL: string = "http://localhost:3000/apartamento/";

  constructor(private http: HttpClient) {}

  newApartamentoToAPI(data: Apartamento): Observable<any> {
    return this.http.post<any>(this._URL, data);
  }

  getApartamentoFromAPI(): Observable<Apartamento[]> {
    return this.http.get<Apartamento[]>(this._URL);
  }

  getApartamentoAtivosFromAPI(): Observable<Apartamento[]> {
    return this.http.get<Apartamento[]>(this._URL + "activos");
  }

  getApartamentobyId(id): Observable<Apartamento[]> {
    return this.http.get<Apartamento[]>(this._URL + "id/" + id);
  }

  getApartamentobyUserId(email): Observable<Apartamento[]> {
    return this.http.get<Apartamento[]>(this._URL + "dono/" + email);
  }

  deleteApartamento(id:string) {
    return this.http.delete<any>(this._URL + id);
  }

  updateApartamento(id:string, data) {
    return this.http.patch<any>(this._URL + id, data);
  }
}
