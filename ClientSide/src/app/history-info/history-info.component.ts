import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { HistoricoService } from "../historico.service";

@Component({
  selector: "app-history-info",
  templateUrl: "./history-info.component.html",
  styleUrls: ["./history-info.component.css"]
})
export class HistoryInfoComponent implements OnInit {
  id = "";
  histo: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private historico: HistoricoService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get("id");
    });

    this.historico.getHistoricobyId(this.id).subscribe(res => {
      this.histo = res["historico"];
    });
  }

  validar() {
    let data = [];
    if (this.histo.estado != "disponivel") {
      data = [{ propName: "estado", value: "disponivel" }];
    } else {
      data = [{ propName: "estado", value: "indisponivel" }];
    }
    data.push({propName:"email" , value: this.histo.userId.email});
    
    this.historico.updateHistorico(this.id, data).subscribe(res => {
      if (this.histo.estado != "disponivel") {
        window.alert("a reserva foi aceite");
      } else {
        window.alert("a reserva foi rejeitada");
      }
    });
  }
}
