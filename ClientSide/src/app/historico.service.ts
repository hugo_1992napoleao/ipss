import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Historico } from './historico';

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  private _URL: string = "http://localhost:3000/historico/";

  constructor(private http: HttpClient) { }

  getHistoricoFromAPI(): Observable<Historico[]> {
    return this.http.get<Historico[]>(this._URL);
  }

  newHistoricoToAPI(data: Historico): Observable<Historico[]> {
    return this.http.post<Historico[]>(this._URL, data);
  }

  deleteHistorico(id:string) {
    return this.http.delete<any>(this._URL + id);
  }

  getHistoricobyId(id): Observable<Historico[]> {
    return this.http.get<Historico[]>(this._URL + "/id/" + id);
  }

  getHistoricobyApartamentoId(id): Observable<Historico[]> {
    return this.http.get<Historico[]>(this._URL + "/apartamento/" + id);
  }

  getHistoricobyIdUser(id): Observable<Historico[]> {
    return this.http.get<Historico[]>(this._URL + "/user/" + id);
  }

  updateHistorico(id:string, data) {
    return this.http.patch<any>(this._URL + id, data);
  }
}
