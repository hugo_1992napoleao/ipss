//Modolos 
const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
//Modolos criados por nos
const apartamentoRoutes = require('./api/routes/Apartamento');
const usersRoutes = require('./api/routes/utilizadores');
const historicoRoutes = require('./api/routes/historico');

//midleware
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());
app.use('/uploads', express.static('uploads'));

//Rotas que vão lidar com requests
app.use('/apartamento', apartamentoRoutes);
app.use('/utilizador', usersRoutes);
app.use('/historico', historicoRoutes);

//Ligação a base de dados
mongoose.connect('mongodb+srv://admin:'+ process.env.MONGO_ATLAS_PW +'@personalnode-bda8o.azure.mongodb.net/test?retryWrites=true&w=majority', {useNewUrlParser: true});

//Tratar de erros
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;