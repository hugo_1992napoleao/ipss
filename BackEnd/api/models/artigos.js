const mongoose = require('mongoose');

const artigoSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, require: true},
    endSale: {type: Date, require: true},
    img: {type: String, default: 'no img'},
    price: {type: Number, default: -1},
    description: {type: String, require: true},
    winner: {type: String, default: ''},
    estado: {type: String, default: 'pedente'},
    owner: {type: String, require: true}
});

module.exports = mongoose.model('Artigo', artigoSchema);