const mongoose = require('mongoose');

const historicoSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: { type: mongoose.Schema.Types.ObjectId, ref:"Utilizador", require: true},
    apartamentoId: {type: mongoose.Schema.Types.ObjectId, ref: "Apartamento", require: true},
    inicio: { type: String, require: true },
    fim: { type: String, require: true },
    estado: {type: String, default:"pendete"}
});

module.exports = mongoose.model('historico', historicoSchema);