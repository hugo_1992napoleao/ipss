const mongoose = require('mongoose');

const apartamentoSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nome: {type: String},
    morada: {type: Object},
    lotacao: {type: Number},
    preco: {type: Number},
    estado: {type: String},
    caracteristicas: {type: Array},
    servicos: {type: Array},
    dono: {type: String},
    multimedia: {type: String}
});

module.exports = mongoose.model('Apartamento', apartamentoSchema);