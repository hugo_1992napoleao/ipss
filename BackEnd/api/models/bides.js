const mongoose = require('mongoose');

const bidesSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    owner: {type: mongoose.Schema.Types.ObjectId, ref: 'Utilizador', require: true},
    artigo: {type: mongoose.Schema.Types.ObjectId, ref: 'Artigo', require: true},
    value: {type: Number, default: true},
    date: {type: Date, require: true}
});

module.exports = mongoose.model('bide', bidesSchema);