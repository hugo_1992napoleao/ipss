//Modulos importados
const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const nodemailer = require("nodemailer");
//Modulos feitos por nos
const historicoModel = require("../models/historico");
const apartamentoModule = require("../models/apartamento");
const userModule = require("../models/utilizadores");

/**
 * Metodo responsavel por criar uma nova entrada na
 * base de dados historicos
 */
router.post("/", (req, res, next) => {
  userModule.findById(req.body.userId).then(user => {
    if (!user) {
      return res.status(404).json({
        message: "Utilizador não encontrado"
      });
    }
    apartamentoModule
      .findById(req.body.apartamentoId)
      .then(apartamento => {
        if (!apartamento) {
          return res.status(404).json({
            message: "Apartamento não encontrado"
          });
        }
        const historico = new historicoModel({
          _id: new mongoose.Types.ObjectId(),
          userId: req.body.userId,
          apartamentoId: req.body.apartamentoId,
          inicio: req.body.inicio,
          fim: req.body.fim
        });
        historico
          .save()
          .then(result => {
            console.log(result);
            res.status(201).json({
              message: "entrada criada com sucesso",
              historico: {
                _id: result.id,
                userId: result.userId,
                apartamentoId: result.apartamentoId,
                inicio: result.inicio,
                fim: result.fim,
                estado: result.estado,
                request: {
                  type: "GET",
                  url: "http://localhost:3000/historico/" + result._id
                }
              }
            });
          })
          .catch(err => {
            console.log(err);
            res.status(500).json({
              error: err
            });
          });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });
});

/**
 * Rota responsavel por atualizar uma entrada
 */
router.patch("/:id", (req, res, next) => {
  const id = req.params.id;
  let email;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
    if (ops.propName == "email") {
      email = ops.value;
    }
  }
  historicoModel
    .update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: "zepedrorafael19941994@gmail.com",
          pass: "666666pc"
        },
        tls: { rejectUnauthorized: false }
      });

      const mailOptions = {
        from: "zepedrorafael19941994@gmail.com",
        to: email,
        subject: "Reserva Apartamento",
        text: "Existem alterações na sua reserva"
      };
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log(error);
        } else {
          console.log("Email enviado: " + info.response);
        }
      });

      res.status(200).json({
        message: "Entrada atualizada",
        url: "http://localhost:3000/historico/" + id
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get("/", (req, res, next) => {
  historicoModel
    .find()
    .select("-__v")
    .populate("userId", "-__v")
    .populate("apartamentoId", "-__v")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        historico: docs.map(doc => {
          return {
            id: doc._id,
            userId: doc.userId,
            apartamentoId: doc.apartamentoId,
            inicio: doc.inicio,
            fim: doc.fim,
            estado: doc.estado,
            request: {
              type: "GET",
              url: "http://localhost:3000/historico/" + doc._id
            }
          };
        })
      };
      console.log(response);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

/**
 * Metodo responsavel encontar uma entrada atravez do id
 * na base de dados artigos
 */
router.get("/id/:Id", (req, res, next) => {
  const id = req.params.Id;
  historicoModel
    .findById(id)
    .select("-__v")
    .populate("userId", "-__v")
    .populate("apartamentoId", "-__v")
    .exec()
    .then(doc => {
      console.log("Vindo da base de dados: " + doc);
      if (doc) {
        res.status(200).json({
          historico: doc,
          request: {
            type: "GET",
            description: "Obter todos os historicos",
            url: "http://localhost:3000/historico"
          }
        });
      } else {
        res.status(404).json({
          message: "Nenhum objecto com este id: " + id + " foi encontrado"
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get("/user/:userId", (req, res, next) => {
  id = req.params.userId;
  historicoModel
    .find({ userId: id })
    .select("-__v")
    .populate("userId", "-__v")
    .populate("apartamentoId", "-__v")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        historico: docs.map(doc => {
          return {
            id: doc._id,
            userId: doc.userId,
            apartamentoId: doc.apartamentoId,
            inicio: doc.inicio,
            fim: doc.fim,
            estado: doc.estado,
            request: {
              type: "GET",
              url: "http://localhost:3000/historico/" + doc._id
            }
          };
        })
      };
      console.log(response);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get("/apartamento/:apartamentoId", (req, res, next) => {
  id = req.params.apartamentoId;
  historicoModel
    .find({ apartamentoId: id })
    .select("-__v")
    .populate("userId", "-__v")
    .populate("apartamentoId", "-__v")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        historico: docs.map(doc => {
          return {
            id: doc._id,
            userId: doc.userId,
            apartamentoId: doc.apartamentoId,
            inicio: doc.inicio,
            fim: doc.fim,
            estado: doc.estado,
            request: {
              type: "GET",
              url: "http://localhost:3000/historico/" + doc._id
            }
          };
        })
      };
      console.log(response);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

/**
 * Rota responsavel por apagar uma entrada
 */
router.delete("/:id", (req, res, next) => {
  const id = req.params.id;
  historicoModel
    .remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "historico apagado",
        request: {
          type: "POST",
          url: "http://localhost:3000/historico",
          body: {
            userId: "String",
            apartamentoId: "String",
            estado: "String",
            inicio: "Date",
            fim: "Date"
          }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});
module.exports = router;
