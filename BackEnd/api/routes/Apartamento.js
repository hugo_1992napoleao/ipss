//Modulos importados
const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});
const upload = multer({ storage: storage });
//Modulos feitos por nos
const apartamentoModule = require("../models/apartamento");

/**
 * Rota responsavel por obter todos os apartamentos
 */
router.get("/", (req, res, next) => {
  apartamentoModule
    .find({})
    .select("-__v")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        artigo: docs.map(doc => {
          return {
            id: doc.id,
            nome: doc.nome,
            morada: {
              cidade: doc.morada.cidade,
              rua: doc.morada.rua
            },
            lotacao: doc.lotacao,
            preco: doc.preco,
            caracteristicas: doc.caracteristicas,
            servicos: doc.servicos,
            estado: doc.estado,
            mutimedia: doc.mutimedia,
            dono: doc.dono,
            request: {
              type: "GET",
              url: "http://localhost:3000/artigo/" + doc._id
            }
          };
        })
      };
      console.log(response);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

/**
 * Rota responsavel por obter todos os apartamentos
 */
router.get("/activos", (req, res, next) => {
    apartamentoModule
      .find({"estado":"disponivel"})
      .select("-__v")
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          artigo: docs.map(doc => {
            return {
              id: doc.id,
              nome: doc.nome,
              morada: {
                cidade: doc.morada.cidade,
                rua: doc.morada.rua
              },
              lotacao: doc.lotacao,
              preco: doc.preco,
              caracteristicas: doc.caracteristicas,
              servicos: doc.servicos,
              estado: doc.estado,
              mutimedia: doc.mutimedia,
              dono: doc.dono,
              request: {
                type: "GET",
                url: "http://localhost:3000/artigo/" + doc._id
              }
            };
          })
        };
        console.log(response);
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });

/**
 * Rota responsavel por cirar um novo apartamento sem multimedia
 */
router.post("/", (req, res, next) => {
  const apartamento = new apartamentoModule({
    _id: new mongoose.Types.ObjectId(),
    nome: req.body.nome,
    morada: {
      cidade: req.body.morada.cidade,
      rua: req.body.morada.rua
    },
    lotacao: req.body.lotacao,
    preco: req.body.preco,
    caracteristicas: req.body.caracteristicas,
    servicos: req.body.servicos,
    estado: req.body.estado,
    dono: req.body.dono,
    mutimedia: "no multimedia"
  });
  console.log(req.body);
  apartamento
    .save()
    .then(result => {
      res.status(201).json({
        message: "Apartamento criado com sucesso",
        apartamento: {
          nome: result.nome,
          morada: {
            cidade: result.morada.cidade,
            rua: result.morada.rua
          },
          lotacao: result.lotacao,
          preco: result.preco,
          carateristicas: result.carateristicas,
          servicos: result.servicos,
          estado: result.estado,
          dono: result.dono,
          mutimedia: result.mutimedia,
          request: {
            type: "GET",
            url: "http://localhost:3000/utilizador/" + result._id
          }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

/**
 * Rota responsavel por apagar um apartamento
 */
router.delete("/:Id", (req, res, next) => {
  const id = req.params.Id;
  apartamentoModule
    .remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "Apartamento apagado",
        request: {
          type: "POST",
          url: "http://localhost:3000/apartamento/",
          body: {
            /*name: 'String',
                    email: 'String',
                    password: 'String',
                    type: 'String'*/
          }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

/**
 * Rota responsavel por atualizar os dados de um utilizador
 */
router.patch("/:Id", (req, res, next) => {
  const id = req.params.Id;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  apartamentoModule
    .update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      console.log(result);
      res.status(200).json({
        message: "Apartamento atualizado",
        url: "http://localhost:3000/apartamento/" + id
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

/**
 * Metodo responsavel encontar uma entrada atravez do id 
 * na base de dados artigos
 */
router.get('/dono/:emailDono', (req, res, next) => {
    const emailDono = req.params.emailDono;
    apartamentoModule.find({dono: emailDono})
    .select('-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        if (doc) {
            res.status(200).json({
                artigo: doc,
                request: {
                    type: "GET",
                    description: 'Obter todos os apartamentos',
                    url: "http://localhost:3000/apartamento"                        
                }
            });            
        } else {
            res.status(404).json({
                message: 'Nenhum objecto com este id: ' + id +  ' foi encontrado'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

/**
 * Metodo responsavel encontar uma entrada atravez do id 
 * na base de dados artigos
 */
router.get('/userId/:Id', (req, res, next) => {
  const id = req.params.Id;
  apartamentoModule.find({dono: id})
  .select('-__v')
  .exec()
  .then(doc => {
      console.log('Vindo da base de dados: ' + doc);
      if (doc) {
          res.status(200).json({
              apartamento: doc,
              request: {
                  type: "GET",
                  description: 'Obter todos os apartamentos',
                  url: "http://localhost:3000/apartamento"                        
              }
          });            
      } else {
          res.status(404).json({
              message: 'Nenhum objecto com este id: ' + id +  ' foi encontrado'
          });
      }
  })
  .catch(err => {
      console.log(err);
      res.status(500).json({
          error: err
      });
  });
});

router.get('/id/:Id', (req, res, next) => {
  const id = req.params.Id;
  apartamentoModule.findById(id)
  .select('-__v')
  .exec()
  .then(doc => {
      console.log('Vindo da base de dados: ' + doc);
      if (doc) {
          res.status(200).json({
              apartamento: doc,
              request: {
                  type: "GET",
                  description: 'Obter todos os apartamentos',
                  url: "http://localhost:3000/apartamento"                        
              }
          });            
      } else {
          res.status(404).json({
              message: 'Nenhum objecto com este id: ' + id +  ' foi encontrado'
          });
      }
  })
  .catch(err => {
      console.log(err);
      res.status(500).json({
          error: err
      });
  });
});


module.exports = router;
