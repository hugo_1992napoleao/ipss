//Modulos importados
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
//Modulos feitos por nos
const bidesModel = require('../models/bides');
const artigoModule = require('../models/artigos');
const userModule = require('../models/utilizadores');

/**
 * 
 */
router.get('/bideArtigo/:artigo', (req, res, next) => {
    const id = req.params.artigo;
    console.log(id);
    bidesModel.find({'artigo': id})
    .select('-__v')
    .populate('owner', '-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        if (doc) {
            res.status(200).json({
                bide: doc,
                request: {
                    type: "GET",
                    description: 'Obter todos as bides',
                    url: "http://localhost:3000/bide"                        
                }
            });            
        } else {
            res.status(404).json({
                message: 'Nenhum objecto com este id: ' + id +  ' foi encontrado'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});


/**
 * 
 */
router.get('/bideuser/:userId', (req, res, next) => {
    const userId = req.params.userId;
    console.log(userId);
    bidesModel.find({'owner': userId})
    .select('-__v')
    .populate('artigo', '-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        if (doc) {
            res.status(200).json({
                bide: doc,
                request: {
                    type: "GET",
                    description: 'Obter todos as bides',
                    url: "http://localhost:3000/bide"                        
                }
            });            
        } else {
            res.status(404).json({
                message: 'Nenhum objecto com este id: ' + id +  ' foi encontrado'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});


/**
 * 
 */
router.get('/', (req, res, next) => {
    bidesModel.find()
    .select('-__v')
    .populate('owner', 'email')
    .populate('artigo', 'name')
    .exec()
    .then(docs => {
        const response = {
            count: docs.length,
            bide: docs.map(doc => {
                return {
                    id: doc.id,
                    owner: doc.owner,
                    artigo: doc.artigo,
                    value: doc.value,
                    price: doc.price,
                    date: doc.date,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/bide/" + doc._id                        
                    }
                }
            })
        };
        console.log(response);
        res.status(200).json(response);
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});



/**
 * Metodo responsavel por criar uma nova entrada na 
 * base de dados artigos
 */
router.post('/', (req, res, next) => {
    userModule.findById(req.body.owner)
    .then(user => {
        if (!user) {
            return res.status(404).json({
                message: 'Utilizador não encotrado'
            });
        }
        const bide = new bidesModel({
            _id: new mongoose.Types.ObjectId(),
            owner: req.body.owner,
            artigo: req.body.artigo,
            value: req.body.value,
            date: Date.now()
        });
        bide
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'licitação criada com sucesso',
                bide: {
                    _id: result.id,
                    owner: result.owner,
                    artigo: result.artigo,
                    value: result.value,
                    date: result.date,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/bide/" + result._id                        
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
    
});

/**
 * 
 */
router.get('/:bideId', (req, res, next) => {
    const id = req.params.bideId;
    bidesModel.findById(id)
    .select('-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        if (doc) {
            res.status(200).json({
                bide: doc,
                request: {
                    type: "GET",
                    description: 'Obter todos as bides',
                    url: "http://localhost:3000/bide"                        
                }
            });            
        } else {
            res.status(404).json({
                message: 'Nenhum objecto com este id: ' + id +  ' foi encontrado'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

router.patch('/:bideId', (req, res, next) => {
    const id = req.params.artigoId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    bidesModel.update({_id: id}, {$set: updateOps})
    .exec()
    .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Licitação atualizada',
            url: "http://localhost:3000/bide/" + id
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
/**
 * Rota responsavel por apagar um utilizador
 */
router.delete('/:bideId', (req, res, next) => {
    const id = req.params.bideId;
    bidesModel.remove({_id: id})
    .exec()
    .then(result => {
        res.status(200).json({
            message:"licitação apagada",
            request: {
                type:'POST',
                url: 'http://localhost:3000/bide/',
                body: {
                    userId: 'String',
                    idArtigo: 'Number',
                    value: 'String'
                }
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

module.exports = router;