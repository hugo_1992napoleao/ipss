//Modulos importados
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads/');
    },
    filename: (req, file, cb) => {
        cb(null,  file.originalname)
    }
});
const upload = multer({storage: storage});
//Modulos feitos por nos
const artigoModule = require('../models/artigos');
const userModule = require('../models/utilizadores');


/**
 * Rota responsavel por obter todos os artigos activos
 */
router.get('/ativos', (req, res, next) => {
    artigoModule.find({'estado': 'Activo'})
    .select('-__v')
    .exec()
    .then(docs => {
        const response = {
            count: docs.length,
            artigo: docs.map(doc => {
                return {
                    id: doc.id,
                    name: doc.name,
                    endSale: doc.endSale,
                    img: doc.img,
                    price: doc.price,
                    description: doc.description,
                    winner: doc.winner,
                    estado: doc.estado,
                    owner: doc.owner,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/artigo/" + doc._id                        
                    }
                }
            })
        };
        console.log(response);
        res.status(200).json(response);
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

/**
 * Rota responsavel por obter todos os artigos
 */
router.get('/', (req, res, next) => {
    artigoModule.find()
    .select('-__v')
    .exec()
    .then(docs => {
        const response = {
            count: docs.length,
            artigo: docs.map(doc => {
                return {
                    id: doc.id,
                    name: doc.name,
                    endSale: doc.endSale,
                    img: doc.img,
                    price: doc.price,
                    description: doc.description,
                    winner: doc.winner,
                    estado: doc.estado,
                    owner: doc.owner,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/artigo/" + doc._id                        
                    }
                }
            })
        };
        console.log(response);
        res.status(200).json(response);
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
/**
 * Metodo responsavel por criar uma nova entrada na 
 * base de dados artigos
 */
router.post('/', upload.single('img'), (req, res, next) => {
    
    userModule.findOne({"_id": req.body.owner})
    .then(user => {
        if (!user) {
            return res.status(404).json({
                message: 'Utilizador não encontrado'
            });
        }
        const artigo = new artigoModule({
            _id: new mongoose.Types.ObjectId(),
            name: req.body.name,
            endSale: req.body.endSale,
            //img: 'http://localhost:3000/' + req.file.path,
            description: req.body.description,
            price: req.body.price,
            winner:  req.body.winner,
            estado: req.body.estado,
            owner: req.body.owner
        });
        artigo
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'artigo criado com sucesso',
                artigo: {
                    _id: result.id,
                    name: result.name,
                    endSale: result.endSale,
                    img: result.img,
                    price: result.price,
                    description: result.description,
                    winner: result.winner,
                    category: result.category,
                    owner: result.owner,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/artigo/" + result._id                        
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
    
});

/**
 * Metodo responsavel encontar uma entrada atravez do id 
 * na base de dados artigos
 */
router.get('/:artigoId', (req, res, next) => {
    const id = req.params.artigoId;
    artigoModule.findById(id)
    .select('-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        if (doc) {
            res.status(200).json({
                artigo: doc,
                request: {
                    type: "GET",
                    description: 'Obter todos os artigos',
                    url: "http://localhost:3000/artigo"                        
                }
            });            
        } else {
            res.status(404).json({
                message: 'Nenhum objecto com este id: ' + id +  ' foi encontrado'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

router.patch('/:artigoId', (req, res, next) => {
    const id = req.params.artigoId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    artigoModule.update({_id: id}, {$set: updateOps})
    .exec()
    .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Artigo atualizado',
            url: "http://localhost:3000/artigo/" + id
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
/**
 * Rota responsavel por apagar um utilizador
 */
router.delete('/:artigoId', (req, res, next) => {
    const id = req.params.artigoId;
    artigoModule.remove({_id: id})
    .exec()
    .then(result => {
        res.status(200).json({
            message:"Artigo apagado",
            request: {
                type:'POST',
                url: 'http://localhost:3000/artigo',
                body: {
                    name: 'String',
                    endSale: 'Date',
                    img: 'File',
                    price:'Number',
                    owner:'String',
                    estado:'String'
                }
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

/**
 * 
 */
router.get('/myArtigos/:userId', (req, res, next) => {
    const id = req.params.userId;
    artigoModule.find({'owner': id})
    .select('-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        if (doc) {
            res.status(200).json({
                artigo: doc,
                request: {
                    type: "GET",
                    description: 'Obter todos as artigos',
                    url: "http://localhost:3000/artigos"                        
                }
            });            
        } else {
            res.status(404).json({
                message: 'Nenhum objecto com este id: ' + id +  ' foi encontrado'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});


module.exports = router;