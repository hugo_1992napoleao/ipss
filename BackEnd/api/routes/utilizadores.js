//Modulos importados
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
//Modulos feitos por nos 
const usersModel = require('../models/utilizadores');

/**
 * Rota responsavel por obter todos os utilizadores 
 */
router.get('/', (req, res, next) => {
    usersModel.find()
    .select('-__v')
    .exec()
    .then(docs => {
        const response = {
            count: docs.length,
            utilizador: docs.map(doc => {
                return {
                    _id: doc._id,
                    name: doc.name,
                    email: doc.email,
                    password: doc.password,
                    type: doc.type,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/utilizador/" + doc._id                        
                    }
                }
            })
        };
        console.log(response);
        res.status(200).json(response);
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
/**
 * Rota responsavel por cirar um novo user
 */
router.post('/', (req, res, next) => {
    const user = new usersModel({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    });
    user.save()
    .then(result => {
        let payload = {subject: result._id};
        let token = jwt.sign(payload, 'secretKey');
        res.status(201).json({
            message: 'Utilizador criado com sucesso',
            jsonWebToken: {token},
            utilizador: {
                name: result.name,
                email: result.email,
                _id: result._id,
                password: result.password,
                request: {
                    type: "GET",
                    url: "http://localhost:3000/utilizador/" + result._id                        
                }
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
/**
 * Rota responsavel por encontar um utilizador pelo seu email
 */
router.get('/:userEmail', (req, res, next) => {
    const email = req.params.userEmail;
    usersModel.findOne({'email': email})
    .select('-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        if (doc) {
            res.status(200).json({
                utilizador: doc,
                request: {
                    type: "GET",
                    description: 'Obter todos os artigos',
                    url: "http://localhost:3000/utilizador/"                        
                }
            });            
        } else {
            res.status(404).json({
                message: 'Nenhum utilizador com este id: ' + id +  ' foi encontrado'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

/**
 * Rota responsavel por encontar um utilizador pelo seu id
 */
router.get('/id/:userId', (req, res, next) => {
    const id = req.params.userId;
    usersModel.findById(id)
    .select('-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        if (doc) {
            res.status(200).json({
                utilizador: doc,
                request: {
                    type: "GET",
                    description: 'Obter todos os artigos',
                    url: "http://localhost:3000/utilizador/"                        
                }
            });            
        } else {
            res.status(404).json({
                message: 'Nenhum utilizador com este id: ' + id +  ' foi encontrado'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

/**
 * Rota responsavel por atualizar os dados de um utilizador
 */
router.patch('/:userId', (req, res, next) => {
    const id = req.params.userId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    usersModel.update({_id: id}, {$set: updateOps})
    .exec()
    .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Utilizador atualizado',
            url: "http://localhost:3000/utilizador/" + id
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
/**
 * Rota responsavel por apagar um utilizador
 */
router.delete('/:userId', (req, res, next) => {
    const id = req.params.userId;
    usersModel.remove({_id: id})
    .exec()
    .then(result => {
        res.status(200).json({
            message:"Utilizador apagado",
            request: {
                type:'POST',
                url: 'http://localhost:3000/utilizador/',
                body: {
                    name: 'String',
                    email: 'String',
                    password: 'String',
                    type: 'String'
                }
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

/**
 * Rota responsavel por encontar um utilizador pelo seu email
 */
router.post('/logar', (req, res, next) => {
    const email = req.body.email;
    console.log(email);
    console.log(req.body.password);
    usersModel.findOne({'email': email})
    .select('-__v')
    .exec()
    .then(doc => {
        console.log('Vindo da base de dados: ' + doc);
        console.log(doc.password != req.body.password);
        if (doc) {
            if (doc.password != req.body.password) {
                res.status(401).send({message:'Credenciais invalidas'});
            } else {
                let tipo = doc.type;
                let id = doc._id;
                let email = doc.email;
                let payload = {subject: doc._id}
                let token = jwt.sign(payload, 'secretKey');
                res.status(200).json({token, tipo, id, email});  
            }
        } else {
            res.status(404).json({
                message: 'Credenciais invalidas'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

module.exports = router;